/**
 * @file countdown_wwu.js
 * @author Jacob Shafer : WWU Office of Admissions
 * Spring 2016
 */
(function ($, Drupal, window, document, undefined) {
	var BackImages;

	/**
	 * Modifies the background image to the given url
	 * @param {String} url : string url to image
	 */
	function setCanvasBackground(url) {
		$('#countdown_canvas').css('background-image', 'url(' + url + ')');
	}

	/**
	 * Creates an initializes the countdown
	 * @param {Date} date : initialized Date object
	 * 		Date object accepts month in range 0-11
	 */
	function createCountdown(date) {
		$('.countdown').countdown( {
			until: date,
			layout: '<ul class="countdown-tabs">' +
				'<li><a>{dn} <span>{dl}</span> </a></li>' +
				'<li><a>{hn} <span>{hl}</span> </a></li>' +
				'<li><a>{mn} <span>{ml}</span> </a></li>' +
				'<li class="last-child"><a>{sn} <span>{sl}</span> </a></li>' +
				'</ul>'
		});
	}

	Drupal.behaviors.countdown_wwu = {
		attach: function (context, settings) {
			var date = Drupal.settings.countdown_wwu.TargetDate;
			BackImages = Drupal.settings.countdown_wwu.BackImages;

			$('#countdown_radio').buttonset();
			setCanvasBackground(BackImages[0]);

			createCountdown(new Date(parseInt(date[0]), parseInt(date[1]) - 1, parseInt(date[2]), 10));

			$('#countdown_radio :radio').each(function (index) {
				$(this).click(function (e) {
					setCanvasBackground(BackImages[index]);
					$(".selected__").removeClass("selected__").addClass("unselected__");
					$('label[for="' + this.id + '"]').removeClass("unselected__").addClass('selected__');
				});
			});
		}
	};
})(jQuery, Drupal, this, this.document);